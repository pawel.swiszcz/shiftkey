<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace App;

use App\Scopes\TripOwnerScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(mixed $validated)
 */
class Trip extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'car_id',
        'date',
        'miles',
    ];

    protected $hidden = [
        'user_id',
        'car_id',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new TripOwnerScope());
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
