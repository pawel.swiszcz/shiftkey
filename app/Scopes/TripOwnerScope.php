<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class TripOwnerScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $id = Auth::id();
        $builder->where('user_id', '=', $id);
    }
}
