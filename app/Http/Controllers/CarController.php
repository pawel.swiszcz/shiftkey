<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): array
    {
        return ['data' => Car::all()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(StoreCarRequest $request)
    {
        // Retrieve the validated input data...
        $validated = $request->validated();

        $id = Auth::id();
        $validated['user_id'] = $id;

        Car::create($validated);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): array
    {
        return ['data' => Car::findOrFail($id)];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(Car $car)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(UpdateCarRequest $request, Car $car)
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): void
    {
        $car = Car::findOrFail($id);
        $car->delete();
    }
}
