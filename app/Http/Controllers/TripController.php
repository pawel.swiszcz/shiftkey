<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\StoreTripRequest;
use App\Http\Requests\UpdateTripRequest;
use App\Trip;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws \Exception
     */
    public function store(StoreTripRequest $request)
    {
        $validated = $request->validated();

        $id = Auth::id();
        $validated['user_id'] = $id;

        $validated['date'] = (new Carbon($validated['date']))->format('Y-m-d');

        DB::beginTransaction();

        try {
            Trip::create($validated);

            $car = Car::findOrFail($validated['car_id']);

            $car->miles += $validated['miles'];
            ++$car->trip_count;

            $car->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show(Trip $trip)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(Trip $trip)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(UpdateTripRequest $request, Trip $trip)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy(Trip $trip)
    {
    }
}
