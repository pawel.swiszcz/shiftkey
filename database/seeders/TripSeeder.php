<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            [
                'id' => 1,
                'date' => Carbon::now()->subDays(1)->format('Y-m-d'),
                'miles' => 11.3,
                'car_id' => 1,
            ],
            [
                'id' => 2,
                'date' => Carbon::now()->subDays(2)->format('Y-m-d'),
                'miles' => 12.0,
                'car_id' => 4,
            ],
            [
                'id' => 3,
                'date' => Carbon::now()->subDays(3)->format('Y-m-d'),
                'miles' => 6.8,
                'car_id' => 1,
            ],
            [
                'id' => 4,
                'date' => Carbon::now()->subDays(4)->format('Y-m-d'),
                'miles' => 5,
                'car_id' => 2,
            ],
            [
                'id' => 5,
                'date' => Carbon::now()->subDays(5)->format('Y-m-d'),
                'miles' => 10.3,
                'car_id' => 3,
            ],
        ]);
    }
}
