<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cars', static function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cars', static function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
