<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Tests\Feature;

use App\Car;
use App\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class AddTripTest extends TestCase
{
    public function testAUserCanAddATrip(): void
    {
        $user = new User();
        $user->name = 'John Doe 2';
        $user->email = Str::random(10).'@'.Str::random(10).'.com';
        $user->password = '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm';
        $user->save();

        $car = new Car();
        $car->user_id = $user->id;
        $car->make = 'Toyota';
        $car->model = 'Camry';
        $car->year = 2022;
        $car->save();

        $this->actingAs($user, 'web')
            ->withSession([])->withToken($user->createToken('laravel_token', ['*'])->accessToken)
            ->postJson('/api/add-trip', [
                'miles' => 99,
                'car_id' => $car->id,
                'date' => '2023-02-05T23:00:00.000Z',
            ]);

        $this->assertDatabaseHas('trips', [
            'miles' => 99,
            'car_id' => $car->id,
            'user_id' => $user->id,
            'date' => '2023-02-05',
        ]);
    }
}
