<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    public function testHomePage(): void
    {
        $user = User::create([
            'email' => Str::random(10).'@'.Str::random(10).'.com',
            'name' => 'test',
            'password' => 'password',
        ]);

        $response = $this->actingAs($user, 'web')
            ->withSession([])
            ->get('/home');

        $response->assertStatus(200);
    }
}
