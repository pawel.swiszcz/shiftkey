<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
