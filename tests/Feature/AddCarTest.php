<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Tests\Feature;

use App\Car;
use App\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class AddCarTest extends TestCase
{
    public function testAUserCanAddACar(): void
    {
        $user = new User();
        $user->name = 'John Doe';
        $user->email = Str::random(10).'@'.Str::random(10).'.com';
        $user->password = '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm';
        $user->save();

        $car = new Car();
        $car->make = 'Toyota';
        $car->model = 'Yaris';
        $car->year = 2022;

        $this->actingAs($user, 'web')
            ->withSession([])->withToken($user->createToken('laravel_token', ['*'])->accessToken)
            ->postJson('/api/add-car', [
                'make' => $car->make,
                'model' => $car->model,
                'year' => $car->year,
            ]);

        $this->assertDatabaseHas('cars', [
            'make' => $car->make,
            'model' => $car->model,
            'year' => $car->year,
        ]);
    }
}
