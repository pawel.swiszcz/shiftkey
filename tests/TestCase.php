<?php
/**
 *
 * Laravel Backend Coding Simulation
 * Trax Milage Tracking Application
 *
 * @author Paweł Świszcz
 * @date 2023-02-05
 *
 */

declare(strict_types=1);

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
